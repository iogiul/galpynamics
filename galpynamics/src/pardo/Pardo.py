from __future__ import division, print_function
import numpy as np
import multiprocessing as mp
from functools import partial

def parallel_helper(R,args):
    func=args[0]
    return func(R,*args[1:])

def parallel_helperRZ(RZ,args):
    R=RZ[:,0]
    Z=RZ[:,1]
    func=args[0]
    return func(R,Z,*args[1:])



#todo It is better to parallel along Z
class ParDo:
    '''
    Manage Multiprocess things
    '''
    def __init__(self, nproc, func=None):
        """Init

        :param nproc: Number of processes
        :param func:  function to parallelize
        """

        self.n=nproc
        self.nproc=nproc


        if func is not None: self.set_func(func=func)

    def set_func(self,func):
        """Set function to parallelize

        :param func: function to parallelize. It should be in the form func(array,*args) where array will be divided in
                chunks in the parallelization. It should return an array with the first column filled with the quantity array
        """

        self.func=func

    def run_grid(self, array, args, _sorted='sort'):
        """Run func in parallel
        It parallelizes the func dividing the first argument in chunks.
        If _sorted=True, the ouput is re-sorted following the input order of the first array
        :param array: Firt argument of function self.func
        :param args: other args
        :param _sorted: If True, sort the output to match the order of array.
                        Otherwise the order is casual and depends on the output order of the chunks.
                        In any case the match between array and results are safe.
        :return: results of func ordered with array as key
        """
        array_chunk = np.array_split(array, self.nproc)
        pff = partial(parallel_helper,args=[self.func,]+list(args))

        with mp.Pool(self.nproc) as pool:
            htab = pool.map(pff, array_chunk)

        results = np.concatenate(htab)

        return results

    def run(self, array1, array2, args, _sorted='input'):
        """Run func in parallel
        It parallelizes the func dividing the first and second argument in chunks.
        If _sorted=True, the ouput is re-sorted following the input order of the first array
        :param array1: First argument of function self.func
        :param array2: Secon argument of function self.func
        :param args: other args
        :param _sorted: If True, sort the output to match the order of array1.
                        Otherwise the order is casual and depends on the output order of the chunks.
                        In any case the match between array and results are safe.
        :return: results of func ordered with array as key
        """

        if len(array1)!=len(array2): raise ValueError('Unequal length')

        RZ = np.zeros(shape=(len(array1),2))
        RZ[:,0] = np.array(array1)
        RZ[:,1] = np.array(array2)
        RZchunk=np.array_split(RZ,self.nproc)
        pff = partial(parallel_helperRZ,args=[self.func,]+list(args))

        with mp.Pool(self.nproc) as pool:
            htab = pool.map(pff, RZchunk)

        results = np.concatenate(htab)

        return results




