#cython: language_level=3, boundscheck=False, cdivision=True, wraparound=False
from libc.math cimport sqrt, log, asin, pow, exp, sinh, tanh
from .general_halo cimport m_calc, potential_core, integrand_core, vcirc_core
from scipy.integrate import quad
from scipy._lib._ccallback import LowLevelCallable
import numpy as np
cimport numpy as np
ctypedef double * double_ptr
ctypedef void * void_ptr
from cython_gsl cimport *

cdef double PI=3.14159265358979323846

cdef double bn_sersic(double n) nogil:
    """
    Sersic b(n) function approximation following https://en.wikipedia.org/wiki/Sérsic_profile
    :param n: Sersic exponent n
    :return: b(n)
    """
    cdef:
        double B=2
        double C=-1./3.
        double A1=4./405.
        double A2=46./25515.
        double A3=131./1148175.
        double A4=-2194697./30690717750.
        double n2=n*n
        double n3=n2*n
        double n4=n3*n

    return B*n+C+(A1/n)+(A2/n2)+(A3/n3)+(A4/n4)


cdef double p_sersic(double n) nogil:
    """
    p(n) function representing the power law exponent of the 3D Sersic profile. 
    This functional form is presented in Terzic&Graham, 2005, https://ui.adsabs.harvard.edu/abs/2005MNRAS.362..197T/abstract
    The authors says that this is valid to reproduce the Sersic profile in the range 0.01<R/Re<1000 and 
    0.6 < n < 10.  
    :param n: Sersic exponent n
    :return: p(n)
    """

    cdef:
        double invn = 1./n

    return 1.0 - 0.6097*invn + 0.05563*invn*invn


cdef double dens_sersic(double m, double d0, double Re, double n)  nogil:
    """
     Equation 4 in Terzic&Graham, 2005, https://ui.adsabs.harvard.edu/abs/2005MNRAS.362..197T/abstract
     :param m: ellipsoidal coordinate
     :param d0: d0, central density   at (R,Z)=(0,0) [Msun/kpc^3]
     :param Re: Re,  effective half light radius [kpc]
     :param n: n, Sersic exponent n            
     :return: density at m
    """

    cdef:
        double x=m/Re
        double b = bn_sersic(n)
        double p = p_sersic(n)

        double plaw = pow(x,-p)
        double Eexp = b*pow(x,1./n)


    return d0*plaw*exp(-Eexp)

cdef double dens_core_sersic(double m, void * params) noexcept nogil:
    """
    Equation to be used to estimate psi (Eq. 2.117 Binney&Tremaine)
    psi = int^m_0 2 m dens(m) dm 
    So this function returns 2 m dens(m)
    :param m: ellipsoidal coordinate
    :param params: three parameters of the density law 
            item0: d0, central density   at (R,Z)=(0,0) [Msun/kpc^3]
            item1: Re,  effective half light radius [kpc]
            item2:  n, Sersic exponent n        
    :return: psi integrand  at m
    """

    cdef:
        double d0 = (<double_ptr> params)[0]
        double Re = (<double_ptr> params)[1]
        double n = (<double_ptr> params)[2]

    return 2*m*dens_sersic(m,d0,Re,n)



cdef double psi_core_sersic_integral(double d0, double Re, double n, double m, double toll) nogil:
    """
    Auxiliary functions (used to estimate the potential) linked to density sersic law
    psi = int^m_0 2 m dens(m) dm (Eq. 2.117 Binney&Tremaine)
    This is estimated numerically  integrating
    :param d0: d0, central density   at (R,Z)=(0,0) [Msun/kpc^3]
    :param Re: Re, effective half light radius [kpc]
    :param n: n, Sersic exponent n    
    :param m: m, ellipsoidal coordinate  
    :param toll: integration tollerance
    :return: value of the psi ued in the potential integration
    """

    cdef:
        double result, error
        gsl_integration_workspace * w
        gsl_function F
        double params[3]

    params[0] = d0
    params[1] = Re
    params[2] = n


    W = gsl_integration_workspace_alloc (10000)


    F.function = &dens_core_sersic
    F.params = params


    gsl_integration_qag(&F, 0, m, toll, toll, 10000, GSL_INTEG_GAUSS15, W, &result, &error)
    gsl_integration_workspace_free(W)

    return result

cdef double psi_core_sersic(double d0, double Re, double n, double m) nogil:
    """
    Auxiliary functions (used to estimate the potential) linked to density sersic law
    psi = int^m_0 2 m dens(m) dm (Eq. 2.117 Binney&Tremaine)
    Considering the 3D Sersic profile: 
    dens = d0 * (x/Re)^-p * exp(-b *(x/Re)^(1/n))
    the integral on x is 
    -2*n*Re*Re*b^-ef*Gamma_inc_upper(ef,b*(r/Re)^(1/n))
    with ef=n*(2-p)
    and Gamma_inc_upper is the upper  incomplete  Gamma function Gamma_inc_upper(a,x) = int^Infty_x e^-t t^(a-1) dt 
    So integrating from 0 to Infinity 
    psi = 2*n*Re*Re*b^-ef * (Gamma(ef) - Gamma(ef,b*(r/Re)^(1/n))), where Gamma(e) is the gamma function Gamma_inc_upper(a) = int^Infty_0 e^-t t^(a-1) dt 
    :param d0: d0, central density   at (R,Z)=(0,0) [Msun/kpc^3]
    :param Re: Re, effective half light radius [kpc]
    :param n: n, Sersic exponent n    
    :param m: m, ellipsoidal coordinate 
    :return:  Psi function at m
    """

    cdef:
        double b = bn_sersic(n)
        double p = p_sersic(n)
        double x = b*pow(m/Re,1./n)
        double exfac = n*(2-p)
        double norm = 2*d0*n*Re*Re*pow(b,-exfac)
        double gamma_inc_lower = gsl_sf_gamma(exfac) - gsl_sf_gamma_inc(exfac,x) #int^x_0 e^-t t^{a-1} dt

    return norm*gamma_inc_lower



cdef double integrand_sersic(int nn, double *data) nogil:
    """
    Integrand function to estimate the sersic potential
    :param nn:  dummy values
    :param data: An array with the following quantites:
        - data[0]: m, ellipsoidal coordinate 
        - data[1]: R, cylindrical radius  
        - data[2]: z, cylindrical height   
        - data[3]:  mcut, ellipsoidal coordinate for which we assume the density goes to 0 
        - data[4]: d0, central density at (R=0, z=0)
        - data[5]: Re, effective half-light radius 
        - data[6]: n, Sersic exponent n
        - data[7]: e, halo ellipticity
    :return:  the potential integrand at m,R,z,e
    """

    cdef:
        double m = data[0]

    if m==0.: return 0 #Xi diverge to infinity when m tends to 0, but the integrand tends to 0

    cdef:
        double R = data[1]
        double Z = data[2]
        double mcut = data[3]
        double d0 = data[4]
        double Re = data[5]
        double n = data[6]
        double e = data[7]
        double psi, result #, num, den

    if (m<=mcut): psi=psi_core_sersic(d0, Re, n, m)
    else: psi=psi_core_sersic(d0, Re, n, mcut)

    result=integrand_core(m, R, Z, e, psi)

    return result


cdef double  _potential_sersic(double R, double Z, double mcut, double d0, double Re, double n, double e,  double toll):
    """
    Core function to estimate the potential 
    :param R: cylindrical radius R
    :param Z:  cylindrical height Z
    :param mcut:   mcut, ellipsoidal coordinate for which we assume the density goes to 0 
    :param d0:  d0, central density at (R=0, z=0)
    :param Re:  Re, effective half-light radius 
    :param n:  n, Sersic exponent n
    :param e:  e, halo ellipticity
    :param toll: integration tollerance 
    :return: Potential at R,Z using Eq. 
    """

    cdef:
        double G=4.498658966346282e-12 #G constant in  kpc^3/(msol Myr^2 )
        double cost=2*PI*G
        double m0
        double psi
        double intpot
        double result

    m0=m_calc(R,Z,e)

    #Integ
    import galpynamics.src.pot_halo.pot_c_ext.sersic_halo as mod
    fintegrand=LowLevelCallable.from_cython(mod,'integrand_sersic')


    intpot=quad(fintegrand,0.,m0,args=(R,Z,mcut,d0,Re,n,e),epsabs=toll,epsrel=toll)[0]

    psi=psi_core_sersic(d0,Re,n,mcut)

    result=potential_core(e, intpot, psi)

    return result

cdef double[:,:]  _potential_sersic_array(double[:] R, double[:] Z, int nlen, double mcut, double d0, double Re, double n, double e, double toll):
    """
    Estimate the potential of an array of R,Z values (R and Z need to have the same dimension)
    :param R: array storying the cylindrical R values
    :param Z:  array storing the cylindrical Z values
    :param nlen: array dimension
    :param mcut: mcut, ellipsoidal coordinate for which we assume the density goes to 0 
    :param d0:   d0, central density at (R=0, z=0)
    :param Re:  Re, effective half-light radius 
    :param n:   n, Sersic exponent n
    :param e:  e, halo ellipticity
    :param toll: integration tollerance 
    :return:  a 2D array with len (lenR x lenZ) storing:
        - first column: R
        - second column: Z
        - third column: potential at R, Z
    """

    cdef:
        double G=4.498658966346282e-12 #G constant in  kpc^3/(msol Myr^2 )
        double cost=2*PI*G
        double m0
        double psi
        double[:,:] ret=np.empty((nlen,3), dtype=np.dtype("d"))
        double intpot
        int i



    #Integ
    import galpynamics.src.pot_halo.pot_c_ext.sersic_halo as mod
    fintegrand=LowLevelCallable.from_cython(mod,'integrand_sersic')



    for  i in range(nlen):

        ret[i,0]=R[i]
        ret[i,1]=Z[i]

        m0=m_calc(R[i],Z[i],e)

        intpot=quad(fintegrand,0.,m0,args=(R[i],Z[i],mcut,d0,Re,n,e),epsabs=toll,epsrel=toll)[0]

        psi = psi_core_sersic(d0, Re, n, mcut)

        ret[i,2]=potential_core(e, intpot, psi)


    return ret


cdef double[:,:]  _potential_sersic_grid(double[:] R, double[:] Z, int nlenR, int nlenZ, double mcut, double d0, double Re, double n,  double e, double toll):
    """
    Estimate the potential of a grid of R,Z values.
    The function will estimate the potential considerin all the combination of R and z
    :param R: array storying the cylindrical R values
    :param Z:  array storing the cylindrical Z values
    :param nlenR: length of the R array
    :param nlenZ: length of the z array
    :param mcut: mcut, ellipsoidal coordinate for which we assume the density goes to 0 
    :param d0:   d0, central density at (R=0, z=0)
    :param Re:  Re, effective half-light radius 
    :param n:   n, Sersic exponent n
    :param e:  e, halo ellipticity
    :param toll: integration tollerance 
    :return:  a 2D array with len (lenR x lenZ) storing:
        - first column: R
        - second column: Z
        - third column: potential at R, Z
    """

    cdef:
        double G=4.498658966346282e-12 #G constant in  kpc^3/(msol Myr^2 )
        double cost=2*PI*G
        double m0
        double psi
        double[:,:] ret=np.empty((nlenR*nlenZ,3), dtype=np.dtype("d"))
        double intpot
        int i, j, c



    #Integ
    import galpynamics.src.pot_halo.pot_c_ext.sersic_halo as mod
    fintegrand=LowLevelCallable.from_cython(mod,'integrand_sersic')

    c=0
    for  i in range(nlenR):
        for j in range(nlenZ):

            ret[c,0]=R[i]
            ret[c,1]=Z[j]

            m0=m_calc(R[i],Z[j],e)

            intpot=quad(fintegrand,0.,m0,args=(R[i],Z[j],mcut,d0,Re,n,e),epsabs=toll,epsrel=toll)[0]

            psi=psi_core_sersic(d0,Re,n,mcut)

            ret[c,2]=potential_core(e, intpot, psi)
            c+=1

    return ret


cpdef potential_sersic(R, Z, d0, Re, n, e, mcut, toll=1e-4, grid=False):
    """
    Wrapper to estimate the potential of a Sersic halo at R-Z handling the different options 
    (single value, grid, array). 
    :param R: cylidrical radial coordinates (could be a float, int or a 1D numpy array) [Rsun]
    :param Z: cylidrical height coordinates (could be a float, int or a 1D numpy array) [Rsun]
    :param d0: Central density [Msun/kpc^3]
    :param Re: Re, effective half-light radius  [kpc]
    :param n: n, Sersic exponent n
    :param e: e, halo ellipticity
    :param mcut: mcut, ellipsoidal coordinate for which we assume the density goes to 0 
    :param toll: integration tollerance 
    :param grid: If true consider any combination of R and Z to define the coordinates where to estimate the potential
    :return:  The potential at R and Z:
        - If R ad Z are single value return just the potential, otherwise:
        - If grid: True, return the potential for all the combination of R and Z as a 2D array with:
            - first column: R
            - second column: Z
            - third column: potential at R, Z
        - else, return the potential for the list of R and Z values as as a 2D array with:
            - first column: R
            - second column: Z
            - third column: potential at R, Z
    """

    if isinstance(R, float) or isinstance(R, int):
        if isinstance(Z, float) or isinstance(Z, int):
            return np.array(_potential_sersic(R=R,Z=Z,mcut=mcut,d0=d0, Re=Re, n=n, e=e,toll=toll))
        else:
            raise ValueError('R and Z have different dimension')
    else:
        if grid:
            R=np.array(R,dtype=np.dtype("d"))
            Z=np.array(Z,dtype=np.dtype("d"))
            return np.array(_potential_sersic_grid( R=R, Z=Z, nlenR=len(R), nlenZ=len(Z), mcut=mcut, d0=d0, Re=Re, n=n, e=e,toll=toll))
        elif len(R)==len(Z):
            nlen=len(R)
            R=np.array(R,dtype=np.dtype("d"))
            Z=np.array(Z,dtype=np.dtype("d"))
            return np.array(_potential_sersic_array( R=R, Z=Z, nlen=len(R), mcut=mcut, d0=d0, Re=Re, n=n, e=e,toll=toll))
        else:
            raise ValueError('R and Z have different dimension')
