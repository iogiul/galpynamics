#cython: language_level=3, boundscheck=False, cdivision=True, wraparound=False


cdef double bn_sersic(double n) nogil
cdef double p_sersic(double n) nogil
cdef double dens_sersic(double m, double d0, double Re, double n) nogil
cdef double dens_core_sersic(double m, void * params) nogil
cdef double psi_core_sersic(double d0, double Re, double n, double m) nogil
cdef double integrand_sersic(int nn, double *data) nogil
cdef double  _potential_sersic(double R, double Z, double mcut, double d0, double Re, double n, double e,  double toll)
cdef double[:,:]  _potential_sersic_array(double[:] R, double[:] Z, int nlen, double mcut, double d0, double Re, double n, double e, double toll)
cdef double[:,:]  _potential_sersic_grid(double[:] R, double[:] Z, int nlenR, int nlenZ, double mcut, double d0, double Re, double n,  double e, double toll)
