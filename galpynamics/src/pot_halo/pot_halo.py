from __future__ import division, print_function
from .pot_c_ext.isothermal_halo import potential_iso,  vcirc_iso
from .pot_c_ext.nfw_halo import potential_nfw, vcirc_nfw
from .pot_c_ext.core_nfw_halo import potential_core_nfw, vcirc_core_nfw
from .pot_c_ext.alfabeta_halo import potential_alfabeta, vcirc_alfabeta
from .pot_c_ext.plummer_halo import potential_plummer, vcirc_plummer
from .pot_c_ext.einasto_halo import potential_einasto, vcirc_einasto
from .pot_c_ext.valy_halo import potential_valy, vcirc_valy
from .pot_c_ext.exponential_halo import potential_exponential, vcirc_exponential
import multiprocessing as mp
from ..pardo.Pardo import ParDo
from ..utility import cartesian
import numpy as np
import sys
from ..utility import G as utG, kms_to_kpcMyr, kpcMyr_to_kms
import warnings


class halo(object):
    """
    Super class for halo potentials
    """
    def __init__(self,d0,rc,e=0,mcut=100):
        """Init

        :param d0:  Central density in Msun/kpc^3
        :param rc:  Scale radius in kpc
        :param e:  eccentricity (sqrt(1-b^2/a^2))
        :param mcut: elliptical radius where dens(m>mcut)=0
        """
        #TODO This should be a shared constant, instead at the moment is redefined everytime it is called
        self.G=utG #G constant in  kpc^3/(msol Myr^2 )
        self.kms_to_kpcMyr = kms_to_kpcMyr #Factor to pass from km/s to kpc/Myr
        self.kpcMyr_to_kms= kpcMyr_to_kms  #Factor to pass from kpc/Myr to km/s



        self.d0=d0
        self.rc=rc
        self.e=e
        self.toll=1e-4
        self.mcut=mcut
        self.name='General halo'

    def set_toll(self,toll):
        """Set tollerance for quad integration

        :param toll: tollerance for quad integration
        :return:
        """

        self.toll=toll

    def set_mcut(self,mcut):
        """Set mcut

        :param mcut: elliptical radius where dens(m>mcut)=0
        :return:
        """

        self.mcut=mcut

    def potential(self,R,Z=0,grid=False,toll=1e-4,mcut=None, nproc=1,output='1D'):
        """Calculate potential at coordinate (R,Z). If R and Z are arrays with unequal lengths or
            if grid is True, the potential will be calculated in a 2D grid in R and Z.

        :param R: Cylindrical radius [kpc]
        :param Z: Cylindrical height [kpc]
        :param grid:  if True calculate the potential in a 2D grid in R and Z
        :param toll: tollerance for quad integration
        :param mcut: elliptical radius where dens(m>mcut)=0
        :param nproc: Number of processes
        :return:  An array with:
            0-R [kpc]
            1-Z [kpc]
            2-Potential [kpc^2/Myr^2]
        """

        if output=='1D': Dgrid=False
        elif output=='2D': Dgrid=True
        else: raise NotImplementedError('output type \'%s\' not implemented for halo.potential'%str(output))

        if isinstance(R,float) or isinstance(R, int): R=np.array((R,))
        if isinstance(Z,float) or isinstance(Z, int): Z=np.array((Z,))




        if len(R) != len(Z) or grid == True:

            if len(R) != len(Z) and grid == False:
                warnings.warn('\n*WARNING*: estimate potential on model %s. \n'
                      'R and Z have different dimensions, but grid=False. R and Z will be sorted and grid set to True.\n'%self.name)
                sys.stdout.flush()

            ndim = len(R) * len(Z)
            R=np.sort(R)
            Z=np.sort(Z)
            grid=True
        else:
            ndim = len(R)
            if Dgrid==True: raise ValueError('Cannot use output 2D with non-grid input')


        if mcut is None:
            mcut=self.mcut
        else:
            self.mcut=mcut

        if nproc==1 or ndim<1000:
            ret_array = self._potential_serial(R=R,Z=Z,grid=grid,toll=toll,mcut=mcut)
        else:
            ret_array = self._potential_parallel(R=R, Z=Z, grid=grid, toll=toll, mcut=mcut,nproc=nproc)

        if grid and Dgrid:

            ret_Darray=np.zeros((3,len(R),len(Z)))
            ret_Darray[0,:,:]=ret_array[:,0].reshape(len(R),len(Z))
            ret_Darray[1,:,:]=ret_array[:,1].reshape(len(R),len(Z))
            ret_Darray[2,:,:]=ret_array[:,2].reshape(len(R),len(Z))

            return ret_Darray

        else:

            return ret_array


    def _potential_serial(self,R,Z,grid=False,toll=1e-4,mcut=None):
        """Calculate the potential in R and Z using a serial code

        :param R: Cylindrical radius [kpc]
        :param Z: Cylindrical height [kpc]
        :param grid:  if True calculate the potential in a 2D grid in R and Z
        :param toll: tollerance for quad integration
        :param mcut: elliptical radius where dens(m>mcut)=0
        :return:
        """

        raise NotImplementedError('Potential serial not implemented for this class')

    def _potential_parallel(self, R, Z, grid=False, toll=1e-4, mcut=None,nproc=2):
        """Calculate the potential in R and Z using a parallelized code.

        :param R: Cylindrical radius [kpc]
        :param Z: Cylindrical height [kpc]
        :param grid:  if True calculate the potential in a 2D grid in R and Z
        :param toll: tollerance for quad integration
        :param mcut: elliptical radius where dens(m>mcut)=0
        :return:
        """
        raise NotImplementedError('Potential parallel not implemented for this class')

    def vcirc(self, R, toll=1e-4, nproc=1, mcut=None):
        """Calculate Vcirc at planare radius coordinate R.
        :param R: Cylindrical radius [kpc]
        :param toll: tollerance for quad integration
        :param nproc: Number of processes
        :param mcut: elliptical radius where dens(m>mcut)=0
        :return:  An array with:
            0-R
            1-Vcirc
        """

        if mcut is None: mcut=self.mcut

        ndim=len(R)
        if nproc==1 or ndim<100000:
            return self._vcirc_serial(R=R,toll=toll,mcut=mcut)
        else:
            return self._vcirc_parallel(R=R, toll=toll, nproc=nproc,mcut=mcut)

    def _vcirc_serial(self, R, toll=1e-4,mcut=None):
        """Calculate the Vcirc in R using a serial code
        :param R: Cylindrical radius [kpc]
        :param toll: tollerance for quad integration
        :param mcut: elliptical radius where dens(m>mcut)=0
        :return:
        """

        raise NotImplementedError('Vcirc serial not implemented for this class')


    def _vcirc_parallel(self, R, toll=1e-4, nproc=1,mcut=None):
        """Calculate the Vcirc in R using a parallelized code
        :param R: Cylindrical radius [kpc]
        :param toll: tollerance for quad integration
        :param nproc: Number of processes
        :param mcut: elliptical radius where dens(m>mcut)=0
        :return:
        """
        raise NotImplementedError('Potential parallel not implemented for this class')

    def _mdens(self,m):
        """
        Estiamte the density at the elliptical radius m
        :param m: elliptical radius [kpc]
        :return: Density at the elliptical radius [Msun/kpc^3]
        """

        raise NotImplementedError('Density not implemented for this class %s'%self.name)


    def _dens(self, R, Z=0, extend_over_mcut=False):
        """
        Estimate the density at the cylindrical coordinate R,Z
        :param R: Cylindrical radius (float or iterable) [kpc]
        :param Z: Cylindircal height (float or iterable) [kpc]
        :param extend_over_mcut: If False the density for m>mcut is set to 0 (m=sqrt(R*R+Z*Z/(1-e^2)), if True do not consider mcut
        :return:  Density at point(s)  [Msun/kpc^3]
        """

        #Estimate elliptical radius
        q2 = 1 - self.e * self.e
        m = np.sqrt(R * R + Z * Z / q2)

        fdens =  self._mdens(m)

        if extend_over_mcut:
            return fdens
        else:
            return np.where(m>self.mcut,0.,fdens)



    def dens(self, R, Z=None, grid=False,output='1D',extend_over_mcut=False):
        """
        Evaulate the density at the point (R,Z)
        :param R: float int or iterable [kpc]
        :param z: float int or iterable, if Z is None R=m elliptical radius (m=sqrt(R*R+Z*Z/(1-e^2)) if e=0 spherical radius) [kpc]
        :param grid:  if True calculate the potential in a 2D grid in R and Z, if len(R)!=len(Z) grid is True by default
        :param output: Control the output type:
            - 1D, it returns a 2D numpy array with R,Z, density
            - 2D, this returns a 3D array in which the first two slice contains
                the equivalent of the results of numpy.meshgrid(R,Z), while
                the third slice contain the density in this R,Z grid.
                This kind of ouput can be used to easily plot a 2D image of the density using imshow
        :param extend_over_mcut: If False the density for m>mcut is set to 0 (m=sqrt(R*R+Z*Z/(1-e^2)), if True do not consider mcut
        :return:  2D array with:
            If Z is None:
                col-0 R, [kpc]
                col-1 dens(m)  [Msun/kpc^3]
            else:
                if output="1D":
                    a 2D numpy array with
                    col-0 R, [kpc]
                    col-1 Z, [kpc]
                    col-1 dens(m)  [Msun/kpc^3]
                else if  output="2D":
                    a 3D numpy array (i,j,k)
                    index i:
                        0- R coordinates [kpc]
                        1- Z coordinates [kpc]
                        2- density at R,Z [Msun/kpc^3]
                    index j:
                        R coordinate
                    index z:
                        Z cordinate
        """

        if output=='1D': Dgrid=False
        elif output=='2D': Dgrid=True
        else: raise NotImplementedError('output type \'%s\' not implemented for halo.dens'%str(output))

        if isinstance(R, int) or isinstance(R, float):  R = np.array([R, ])

        if Z is not None:

            if isinstance(Z, int) or isinstance(Z, float):  Z = np.array([Z, ])


            if grid==True or len(R)!=len(Z):

                if len(R) != len(Z) and grid == False:
                    warnings.warn('\n*WARNING*: estimate potential on model %s. \n'
                          'R and Z have different dimensions, but grid=False. R and Z will be sorted and grid set to True.\n' % self.name)
                    sys.stdout.flush()
                    R=np.sort(R)
                    Z=np.sort(Z)
                    grid=True

                ret=np.zeros(shape=(len(R)*len(Z),3))

                coord=cartesian(R,Z)
                ret[:,:2]=coord
                ret[:,2]=self._dens(coord[:,0],coord[:,1],extend_over_mcut=extend_over_mcut)

                if Dgrid:

                    ret_Darray = np.zeros((3, len(R), len(Z)))
                    ret_Darray[0, :, :] = ret[:, 0].reshape(len(R), len(Z))
                    ret_Darray[1, :, :] = ret[:, 1].reshape(len(R), len(Z))
                    ret_Darray[2, :, :] = ret[:, 2].reshape(len(R), len(Z))

                    ret=ret_Darray


            else:
                if Dgrid == True:
                    raise ValueError('Cannot use output 2D with non-grid input')
                ret=np.zeros(shape=(len(R),3))
                ret[:,0]=R
                ret[:,1]=Z
                ret[:,2]=self._dens(R,Z,extend_over_mcut=extend_over_mcut)

        else:

            ret=np.zeros(shape=(len(R),2))

            ret[:,0]=R
            ret[:,1]=self._dens(R,extend_over_mcut=extend_over_mcut)

        return ret

    def mmass(self, m):

        return self._mass(m)

    def vcirc_spherical(self, r, mcut=None):
        """
        For a spherical distribution, Vc(r)=np.sqrt(G M(r)/r)
        :param R: Cylindrical radius R [kpc]
        :param Z: Cylindrical height Z [kpc]
        :return: Vcirc at R, Z for a spherical distribution [km/s]
        """
        if self.e > 0: raise ValueError("%s: potential_spherical not allowed for non spherical halos " % self.name)

        if mcut is None: mcut=self.mcut

        G = 4.302113488372941e-06  # kpc km2/(msol s^2) To have the V in km/s

        Mmcut = self.mmass(mcut)
        Mr=np.where(r>mcut, Mmcut, self.mmass(r))


        return np.sqrt(G*Mr/r)

    def forceR(self,R,Z,grid=False, hrel=1e-5,  force_constant_h=False, toll=1e-4,mcut=None, nproc=1):
        """
        Estimate the Force along the R direction at R-Z taking the numerical derivative
        of the potential. The derivative is estimate using the midpoint rules, so for each R value
        (or for each couple of R-Z, if grid=True) 2 potential evolutions are required.
        :param R:  Cylindrical radius [kpc]
        :param Z: Cylindrical height [kpc]
        :param grid:  if True calculate the potential in a 2D grid in R and Z
        :param hrel: Relative multiplier used to define the numerical derivative step.
        The step h is equal to h=hrel*R.
        :param force_constant_h: if False, use variable radial step for the potential estimate as defined by
        hrel, otherwise use a constant h equal to the smallest of the steps defined by hrel.
        :param toll: tollerance for the potential integration
        :param mcut: elliptical radius where dens(m>mcut)=0
        :param nproc: Number of parallel processes
        :return:  2D numpy array with:
            - R [kpc]
            - Z [kpc]
            - Force along the cylindrical radial direction in kpc/Myr^2
        """

        R=np.atleast_1d(R)

        hrelR = hrel*R
        if force_constant_h: hrelR=np.min(hrelR)

        Rup  = R + hrelR #Evalutation at x2=x1+h
        Rlow = R - hrelR #Evaluation at x0=x1-h

        #Estimate the potential at the needed points
        PhiRup = self.potential(Rup, Z=Z, grid=grid,toll=toll, mcut=mcut,nproc=nproc,output='1D')
        PhiRlow = self.potential(Rlow, Z=Z, grid=grid,toll=toll, mcut=mcut,nproc=nproc,output='1D')

        res = np.zeros_like(PhiRup)
        res[:, 0] = PhiRup[:, 0] - hrelR #Retrieve the originalR (this is necessary when grid=True)
        res[:, 1] = PhiRup[:, 1]
        #Estimate the Force using the midpoint rule, just assume that for R=0, FR=0 (given the simmetry)
        res[:, 2] = np.where(res[:, 0]==0, 0, (PhiRup[:, 2] - PhiRlow[:, 2]) / (PhiRup[:, 0]-PhiRlow[:,0]))


        return res

    def forceZ(self,R,Z,grid=False, hrel=1e-5,  force_constant_h=False, toll=1e-4,mcut=None, nproc=1):
        """
        Estimate the Force along the Z direction at R-Z taking the numerical derivative
        of the potential. The derivative is estimate using the midpoint rules, so for each R value
        (or for each couple of R-Z, if grid=True) 2 potential evolutions are required.
        :param R:  Cylindrical radius [kpc]
        :param Z: Cylindrical height [kpc]
        :param grid:  if True calculate the potential in a 2D grid in R and Z
        :param hrel: Relative multiplier used to define the numerical derivative step.
        The step h is equal to h=hrel*Z.
        :param force_constant_h: if False, use variable vertical step for the potential estimate as defined by
        hrel, otherwise use a constant h equal to the smallest of the steps defined by hrel.
        :param toll: tollerance for the potential integration
        :param mcut: elliptical radius where dens(m>mcut)=0
        :param nproc: Number of parallel processes
        :return: Force along the cylindrical vertical direction in kpc/Myr^2
        """

        Z=np.atleast_1d(Z)

        hrelZ = hrel*Z
        if force_constant_h: hrelZ=np.min(hrelZ)

        Zup  = Z + hrelZ #Evalutation at x2=x1+h
        Zlow = Z - hrelZ #Evaluation at x0=x1-h

        # Estimate the potential at the needed points
        PhiZup = self.potential(R, Z=Zup, grid=grid, toll=toll, mcut=mcut, nproc=nproc, output='1D')
        PhiZlow = self.potential(R, Z=Zlow, grid=grid, toll=toll, mcut=mcut, nproc=nproc, output='1D')

        res = np.zeros_like(PhiZup)
        res[:, 0] = PhiZup[:, 0]
        res[:, 1] = PhiZup[:, 1] - hrelZ
        # Estimate the Force using the midpoint rule, just assume that for Z=0, FZ=0 (given the simmetry)
        res[:, 2] = np.where( res[:, 1] == 0, 0, (PhiZup[:, 2] - PhiZlow[:, 2]) / (PhiZup[:,1]-PhiZlow[:,1]))

        return res

    def vcircR(self,R,Z,grid=False, hrel=1e-5,  force_constant_h=False, toll=1e-4,mcut=None, nproc=1):
        """
        Estimate the "pseudo circular velocity) at coordinates R-Z.
        This is the circular velocity a body have in this potential assuming that the vertical forces are zero.
        I.e., VcircR^2/R = FR(R,Z) where R is the force along the R direction.
        Notice, a body with this velocity will not be on a circular orbit since the vertical forces are not zero.
        In a spherical halo, one may anyway put a body on a circular orbit at the position R and Z and the circular
        velocity wull be equivalent to the circular velocity at the spherical radius r=sqrt(R*R+Z*Z).
        NOTICE: This method ALWAYS estimate the VcircR through a numerical derivative of the potential  even if Z=0
        :param R:  Cylindrical radius [kpc]
        :param Z: Cylindrical height [kpc]
        :param grid:  if True calculate VcircR in a 2D grid in R and Z
        :param hrel: Relative multiplier used to define the numerical derivative step.
        The step h is equal to h=hrel*Z.
        :param force_constant_h: if False, use variable vertical step for the estimate of the  potential estimate (that is then
        used for the estimate of the force along the cylindrical radial coordinate).
        hrel, otherwise use a constant h equal to the smallest of the steps defined by hrel.
        :param toll: tollerance for the potential integration
        :param mcut: elliptical radius where dens(m>mcut)=0
        :param nproc: Number of parallel processes
        :return:  2D numpy array with:
            - R [kpc]
            - Z [kpc]
            - Pseudo circular velocity in km/s
        """
        #Estimate the radial force at R-Z (return potential in kpc/Myr^2)
        FR=self.forceR(R=R,Z=Z,grid=grid,hrel=hrel,force_constant_h=force_constant_h,toll=toll,mcut=mcut,nproc=nproc)



        #Now the pseudo cicular velocity is V^2/R = FR, so
        Vkms = np.sqrt(FR[:,0]*FR[:,2])*kpcMyr_to_kms

        res=np.zeros_like(FR)
        res[:,0:2]=FR[:,0:2]

        # Assume the velocity equal 0 at R=0 by simmetry
        res[:, 2] = np.where(res[:,0]==0, 0, Vkms)

        return res



    def __str__(self):

        s=''
        s+='Model: General halo\n'
        s+='d0: %.2f Msun/kpc3 \n'%self.d0
        s+='rc: %.2f\n'%self.rc
        s+='e: %.3f \n'%self.e
        s+='mcut: %.3f \n'%self.mcut

        return s

'''
#TODO: la vcirc dell alone isotermo e analitica per ogni e, implementare la formula nella mia tesi
class isothermal_halo(halo):

    def __init__(self,d0,rc,e=0,mcut=100):
        """Isothermal halo d=d0/(1+r^2/rc^2)

        :param d0:  Central density in Msun/kpc^3
        :param rc:  Scale radius in kpc
        :param e:  eccentricity (sqrt(1-b^2/a^2))
        :param mcut: elliptical radius where dens(m>mcut)=0
        """
        super(isothermal_halo,self).__init__(d0=d0,rc=rc,e=e,mcut=mcut)
        self.name='Isothermal halo'

    def _potential_serial(self, R, Z, grid=False, toll=1e-4, mcut=None):
        """Calculate the potential in R and Z using a serial code

        :param R: Cylindrical radius [kpc]
        :param Z: Cylindrical height [kpc]
        :param grid:  if True calculate the potential in a 2D grid in R and Z
        :param toll: tollerance for quad integration
        :param mcut: elliptical radius where dens(m>mcut)=0
        :return:
        """


        self.set_toll(toll)


        return  potential_iso(R, Z, d0=self.d0, rc=self.rc, e=self.e, mcut=mcut, toll=self.toll, grid=grid)

    def _potential_parallel(self, R, Z, grid=False, toll=1e-4, mcut=None, nproc=2):
        """Calculate the potential in R and Z using a parallelized code.

        :param R: Cylindrical radius [kpc]
        :param Z: Cylindrical height [kpc]
        :param grid:  if True calculate the potential in a 2D grid in R and Z
        :param toll: tollerance for quad integration
        :param mcut: elliptical radius where dens(m>mcut)=0
        :return:
        """

        self.set_toll(toll)


        pardo=ParDo(nproc=nproc)
        pardo.set_func(potential_iso)

        if len(R)!=len(Z) or grid==True:
            
            
            htab=pardo.run_grid(R,args=(Z,self.d0,self.rc,self.e, mcut,self.toll,grid),_sorted='sort')

        else:

            htab = pardo.run(R,Z, args=(self.d0, self.rc, self.e, mcut, self.toll, grid),_sorted='input')


        return htab

    def _vcirc_serial(self, R, toll=1e-4):
        """Calculate the Vcirc in R using a serial code
        :param R: Cylindrical radius [kpc]
        :param toll: tollerance for quad integration
        :return:
        """
        self.set_toll(toll)
        
        
        return np.array(vcirc_iso(R[idx_sort], self.d0, self.rc, self.e, toll=self.toll))

    def _vcirc_parallel(self, R, toll=1e-4, nproc=1):
        """Calculate the Vcirc in R using a parallelized code
        :param R: Cylindrical radius [kpc]
        :param toll: tollerance for quad integration
        :param nproc: Number of processes
        :return:
        """

        self.set_toll(toll)

        pardo=ParDo(nproc=nproc)
        pardo.set_func(vcirc_iso)

        htab=pardo.run_grid(R,args=(self.d0, self.rc, self.e, self.toll), _sorted='input')

        return htab

    def _dens(self, R, Z=0):

        q2=1-self.e*self.e

        m=np.sqrt(R*R+Z*Z/q2)

        x=m/self.rc

        num=self.d0
        den=(1+x*x)

        return num/den

    def _mass(self,m):

        raise NotImplementedError()

    def __str__(self):

        s=''
        s+='Model: Isothermal halo\n'
        s+='d0: %.2e Msun/kpc3 \n'%self.d0
        s+='rc: %.2f\n'%self.rc
        s+='e: %.3f \n'%self.e
        s+='mcut: %.3f \n'%self.mcut

        return s

class NFW_halo(halo):

    def __init__(self,d0,rs,e=0,mcut=100):
        """NFW halo d=d0/((r/rs)(1+r/rs)^2)

        :param d0:  Central density in Msun/kpc^3
        :param rs:  Scale radius in kpc
        :param e:  eccentricity (sqrt(1-b^2/a^2))
        :param mcut: elliptical radius where dens(m>mcut)=0
        """

        self.rs=rs
        super(NFW_halo,self).__init__(d0=d0,rc=rs,e=e,mcut=mcut)
        self.name='NFW halo'

    @classmethod
    def cosmo(cls, c, V200, H=67, e=0, mcut=100):
        """

        :param c:
        :param V200:  km/s
        :param H:  km/s/Mpc
        :return:
        """

        num=14.93*(V200/100.)
        den=(c/10.)*(H/67.)
        rs=num/den

        rho_crit=8340.*(H/67.)*(H/67.)
        lc=np.log(1+c)
        denc=c/(1+c)
        delta_c=(c*c*c) / (lc - denc)
        d0=rho_crit*delta_c


        return cls(d0=d0, rs=rs, e=e, mcut=mcut)

    @classmethod
    def cosmoM(cls, c, M200, H=67, e=0, mcut=100):
        """

        :param c:
        :param M200:  Msun
        :param H:  km/s/Mpc
        :return:
        """


        rho_crit=8340.*(H/67.)*(H/67.)
        lc=np.log(1+c)
        denc=c/(1+c)
        delta_c=(c*c*c) / (lc - denc)
        d0=rho_crit*delta_c

        num=3*M200
        den=(c*c*c)*rho_crit*(800*np.pi)
        rs = num / den


        return cls(d0=d0, rs=rs, e=e, mcut=mcut)


    def _potential_serial(self, R, Z, grid=False, toll=1e-4, mcut=None):
        """Calculate the potential in R and Z using a serial code

        :param R: Cylindrical radius [kpc]
        :param Z: Cylindrical height [kpc]
        :param grid:  if True calculate the potential in a 2D grid in R and Z
        :param toll: tollerance for quad integration
        :param mcut: elliptical radius where dens(m>mcut)=0
        :return:
        """


        self.set_toll(toll)


        return  potential_nfw(R, Z, d0=self.d0, rs=self.rc, e=self.e, mcut=mcut, toll=self.toll, grid=grid)

    def _potential_parallel(self, R, Z, grid=False, toll=1e-4, mcut=None, nproc=2):
        """Calculate the potential in R and Z using a parallelized code.

        :param R: Cylindrical radius [kpc]
        :param Z: Cylindrical height [kpc]
        :param grid:  if True calculate the potential in a 2D grid in R and Z
        :param toll: tollerance for quad integration
        :param mcut: elliptical radius where dens(m>mcut)=0
        :return:
        """

        self.set_toll(toll)


        pardo=ParDo(nproc=nproc)
        pardo.set_func(potential_nfw)

        if len(R)!=len(Z) or grid==True:

            htab=pardo.run_grid(R,args=(Z,self.d0,self.rc,self.e, mcut,self.toll,grid),_sorted='sort')

        else:

            htab = pardo.run(R,Z, args=(self.d0, self.rc, self.e, mcut, self.toll, grid),_sorted='input')


        return htab

    def _vcirc_serial(self, R, toll=1e-4):
        """Calculate the Vcirc in R using a serial code
        :param R: Cylindrical radius [kpc]
        :param toll: tollerance for quad integration
        :return:
        """
        self.set_toll(toll)

        return np.array(vcirc_nfw(R, self.d0, self.rc, self.e, toll=self.toll))

    def _vcirc_parallel(self, R, toll=1e-4, nproc=1):
        """Calculate the Vcirc in R using a parallelized code
        :param R: Cylindrical radius [kpc]
        :param toll: tollerance for quad integration
        :param nproc: Number of processes
        :return:
        """

        self.set_toll(toll)

        pardo=ParDo(nproc=nproc)
        pardo.set_func(vcirc_nfw)

        htab=pardo.run_grid(R,args=(self.d0, self.rc, self.e, self.toll),_sorted='input')

        return htab

    def _dens(self, R, Z=0):

        q2 = 1 - self.e * self.e

        m = np.sqrt(R * R + Z * Z / q2)

        x = m / self.rs

        num = self.d0
        den = (x)*(1 + x)*(1 + x)

        return num / den


    def _mass(self,m):

        raise NotImplementedError()

    def __str__(self):

        s=''
        s+='Model: NFW halo\n'
        s+='d0: %.2e Msun/kpc3 \n'%self.d0
        s+='rs: %.2f\n'%self.rs
        s+='e: %.3f \n'%self.e
        s+='mcut: %.3f \n'%self.mcut

        return s

class alfabeta_halo(halo):

    def __init__(self,d0,rs,alfa,beta,e=0,mcut=100):
        """
        dens=d0/( (x^alfa) * (1+x)^(beta-alfa))
        :param d0:
        :param rs:
        :param alfa:
        :param beta:
        :param e:
        :param mcut:
        """

        if alfa>=2:
            raise ValueError('alpha must be <2')

        self.rs=rs
        self.alfa=alfa
        self.beta=beta
        super(alfabeta_halo,self).__init__(d0=d0,rc=rs,e=e,mcut=mcut)
        self.name='AlfaBeta halo'

    def _potential_serial(self, R, Z, grid=False, toll=1e-4, mcut=None):
        """Calculate the potential in R and Z using a serial code

        :param R: Cylindrical radius [kpc]
        :param Z: Cylindrical height [kpc]
        :param grid:  if True calculate the potential in a 2D grid in R and Z
        :param toll: tollerance for quad integration
        :param mcut: elliptical radius where dens(m>mcut)=0
        :return:
        """


        self.set_toll(toll)

        return potential_alfabeta(R, Z, d0=self.d0, alfa=self.alfa, beta=self.beta, rc=self.rc, e=self.e, mcut=mcut, toll=self.toll, grid=grid)

    def _potential_parallel(self, R, Z, grid=False, toll=1e-4, mcut=None, nproc=2):
        """Calculate the potential in R and Z using a parallelized code.

        :param R: Cylindrical radius [kpc]
        :param Z: Cylindrical height [kpc]
        :param grid:  if True calculate the potential in a 2D grid in R and Z
        :param toll: tollerance for quad integration
        :param mcut: elliptical radius where dens(m>mcut)=0
        :return:
        """

        self.set_toll(toll)


        pardo=ParDo(nproc=nproc)
        pardo.set_func(potential_alfabeta)

        if len(R)!=len(Z) or grid==True:

            htab=pardo.run_grid(R,args=(Z,self.d0,self.alfa,self.beta,self.rc,self.e, mcut,self.toll,grid),_sorted='sort')

        else:

            htab = pardo.run(R,Z, args=(self.d0, self.alfa, self.beta, self.rc, self.e, mcut, self.toll, grid),_sorted='input')


        return htab

    def _vcirc_serial(self, R, toll=1e-4):
        """Calculate the Vcirc in R using a serial code
        :param R: Cylindrical radius [kpc]
        :param toll: tollerance for quad integration
        :return:
        """
        self.set_toll(toll)

        return np.array(vcirc_alfabeta(R, self.d0, self.rc, self.alfa, self.beta, self.e, toll=self.toll))

    def _vcirc_parallel(self, R, toll=1e-4, nproc=1):
        """Calculate the Vcirc in R using a parallelized code
        :param R: Cylindrical radius [kpc]
        :param toll: tollerance for quad integration
        :param nproc: Number of processes
        :return:
        """

        self.set_toll(toll)

        pardo=ParDo(nproc=nproc)
        pardo.set_func(vcirc_alfabeta)

        htab=pardo.run_grid(R,args=(self.d0, self.rc, self.alfa, self.beta, self.e, self.toll),_sorted='input')

        return htab

    def _dens(self, R, Z=0):

        q2 = 1 - self.e * self.e

        m = np.sqrt(R * R + Z * Z / q2)

        x = m / self.rs

        num  = self.d0
        denA = x**self.alfa
        denB = (1+x)**(self.beta-self.alfa)
        den=denA*denB

        return num / den

    def _mass(self,m):

        raise NotImplementedError()

    def __str__(self):

        s=''
        s+='Model: %s\n'%self.name
        s+='d0: %.2e Msun/kpc3 \n'%self.d0
        s+='rs: %.2f\n'%self.rs
        s+='alfa: %.1f\n'%self.alfa
        s+='beta: %.1f\n'%self.beta
        s+='e: %.3f \n'%self.e
        s+='mcut: %.3f \n'%self.mcut

        return s

class hernquist_halo(alfabeta_halo):

    def __init__(self,d0,rs,e=0,mcut=100):
        """
        dens=d0/( (x) * (1+x)^(3))
        :param d0:
        :param rs:
        :param e:
        :param mcut:
        """

        alfa=1
        beta=4
        super(hernquist_halo,self).__init__(d0=d0,rs=rs,alfa=alfa,beta=beta,e=e,mcut=mcut)
        self.name='Hernquist halo'

    def __str__(self):

        s=''
        s+='Model: %s\n'%self.name
        s+='d0: %.2e Msun/kpc3 \n'%self.d0
        s+='rs: %.2f\n'%self.rs
        s+='e: %.3f \n'%self.e
        s+='mcut: %.3f \n'%self.mcut

        return s

class deVacouler_like_halo(alfabeta_halo):

    def __init__(self,d0,rs,e=0,mcut=100):
        """
        Approximation of the 3D deprojection of the R^(1/4) law (De Vacouler) with alfa-beta model dens=d0/( x^(3/2) * (1+x)^(5/2) )
        :param d0:
        :param rs:
        :param e:
        :param mcut:
        """
        alfa=1.5
        beta=4
        super(deVacouler_like_halo, self).__init__(d0=d0, rs=rs, alfa=alfa, beta=beta, e=e, mcut=mcut)
        self.name = 'deVacouler like halo'


    def __str__(self):

        s=''
        s+='Model: %s\n'%self.name
        s+='d0: %.2e Msun/kpc3 \n'%self.d0
        s+='rs: %.2f\n'%self.rs
        s+='e: %.3f \n'%self.e
        s+='mcut: %.3f \n'%self.mcut

        return s

class plummer_halo(halo):

    def __init__(self,rc,d0=None,mass=None,e=0,mcut=100):
        """
        dens=d0/((1+(r/rs)^2)^-5/2)
        :param rc:
        :param d0:
        :param mass:
        :param e:
        :param mcut:
        """

        cost_mass=(4./3.)*np.pi*rc*rc*rc*(np.sqrt(1-e*e))

        if (d0 is None) and (mass is None):
            raise ValueError('d0 or mass must be set')
        elif mass is None:
            mass=d0*cost_mass
        else:
            d0=mass/cost_mass

        super(plummer_halo,self).__init__(d0=d0,rc=rc,e=e,mcut=mcut)
        self.name='Plummer halo'
        self.mass=mass

    def _potential_serial(self, R, Z, grid=False, toll=1e-4, mcut=None):
        """Calculate the potential in R and Z using a serial code

        :param R: Cylindrical radius [kpc]
        :param Z: Cylindrical height [kpc]
        :param grid:  if True calculate the potential in a 2D grid in R and Z
        :param toll: tollerance for quad integration
        :param mcut: elliptical radius where dens(m>mcut)=0
        :return:
        """


        self.set_toll(toll)

        return potential_plummer(R, Z, d0=self.d0, rc=self.rc, e=self.e, mcut=mcut, toll=self.toll, grid=grid)

    def _potential_parallel(self, R, Z, grid=False, toll=1e-4, mcut=None, nproc=2):
        """Calculate the potential in R and Z using a parallelized code.

        :param R: Cylindrical radius [kpc]
        :param Z: Cylindrical height [kpc]
        :param grid:  if True calculate the potential in a 2D grid in R and Z
        :param toll: tollerance for quad integration
        :param mcut: elliptical radius where dens(m>mcut)=0
        :return:
        """

        self.set_toll(toll)


        pardo=ParDo(nproc=nproc)
        pardo.set_func(potential_plummer())

        if len(R)!=len(Z) or grid==True:

            htab=pardo.run_grid(R,args=(Z,self.d0, self.rc,self.e, mcut,self.toll,grid),_sorted='sort')

        else:

            htab = pardo.run(R,Z, args=(self.d0, self.rc, self.e, mcut, self.toll, grid),_sorted='input')


        return htab

    def _vcirc_serial(self, R, toll=1e-4):
        """Calculate the Vcirc in R using a serial code
        :param R: Cylindrical radius [kpc]
        :param toll: tollerance for quad integration
        :return:
        """
        self.set_toll(toll)

        return np.array(vcirc_plummer(R, self.d0, self.rc, self.e, toll=self.toll))

    def _vcirc_parallel(self, R, toll=1e-4, nproc=1):
        """Calculate the Vcirc in R using a parallelized code
        :param R: Cylindrical radius [kpc]
        :param toll: tollerance for quad integration
        :param nproc: Number of processes
        :return:
        """

        self.set_toll(toll)

        pardo=ParDo(nproc=nproc)
        pardo.set_func(vcirc_plummer)

        htab=pardo.run_grid(R,args=(self.d0, self.rc, self.e, self.toll),_sorted='input')

        return htab

    def _mass(self,m):

        raise NotImplementedError()

    def _dens(self, R, Z=0):

        q2 = 1 - self.e * self.e

        m = np.sqrt(R * R + Z * Z / q2)

        x = m / self.rc

        num = self.d0
        den = (1 + x * x)**(2.5)

        return num / den


    def __str__(self):

        s=''
        s+='Model: %s\n'%self.name
        s+='Mass: %.2e Msun \n'%self.mass
        s+='d0: %.2e Msun/kpc3 \n'%self.d0
        s+='rc: %.2f\n'%self.rc
        s+='e: %.3f \n'%self.e
        s+='mcut: %.3f \n'%self.mcut

        return s

class einasto_halo(halo):

    def __init__(self,d0,rs,n,e=0,mcut=100):
        """einasto halo d=d0*exp(-dn*(r/rs)^(1/n))

        :param d0:  Central density in Msun/kpc^3
        :param rs:  Scale radius in kpc
        :param n:
        :param e:  eccentricity (sqrt(1-b^2/a^2))
        :param mcut: elliptical radius where dens(m>mcut)=0
        """

        self.rs=rs
        super(einasto_halo,self).__init__(d0=d0,rc=rs,e=e,mcut=mcut)
        dnn=self.dn(n)
        self.de=self.d0/np.exp(dnn)
        self.n=n
        self.name='Einasto halo'

    @classmethod
    def de(cls,de,rs,n,e=0,mcut=100):

        dnn=cls.dn(n)
        d0=de*np.exp(dnn)

        return cls(d0=d0, rs=rs, n=n, e=e, mcut=mcut)



    def _potential_serial(self, R, Z, grid=False, toll=1e-4, mcut=None):
        """Calculate the potential in R and Z using a serial code

        :param R: Cylindrical radius [kpc]
        :param Z: Cylindrical height [kpc]
        :param grid:  if True calculate the potential in a 2D grid in R and Z
        :param toll: tollerance for quad integration
        :param mcut: elliptical radius where dens(m>mcut)=0
        :return:
        """


        self.set_toll(toll)


        return  potential_einasto(R, Z, d0=self.d0, rs=self.rc, n=self.n, e=self.e, mcut=mcut, toll=self.toll, grid=grid)

    def _potential_parallel(self, R, Z, grid=False, toll=1e-4, mcut=None, nproc=2):
        """Calculate the potential in R and Z using a parallelized code.

        :param R: Cylindrical radius [kpc]
        :param Z: Cylindrical height [kpc]
        :param grid:  if True calculate the potential in a 2D grid in R and Z
        :param toll: tollerance for quad integration
        :param mcut: elliptical radius where dens(m>mcut)=0
        :return:
        """

        self.set_toll(toll)


        pardo=ParDo(nproc=nproc)
        pardo.set_func(potential_einasto)

        if len(R)!=len(Z) or grid==True:

            htab=pardo.run_grid(R,args=(Z,self.d0,self.rc, self.n, self.e, mcut,self.toll,grid),_sorted='sort')

        else:

            htab = pardo.run(R,Z, args=(self.d0, self.rc, self.n, self.e, mcut, self.toll, grid),_sorted='input')
        cpdef

        return htab

    @staticmethod
    def dn(n):

        n2=n*n
        n3=n2*n
        n4=n3*n
        a0=3*n
        a1 = -1. / 3.
        a2 = 8. / (1215. * n)
        a3 = 184. / (229635. * n2)
        a4 = 1048 / (31000725. * n3)
        a5 = -17557576 / (1242974068875. * n4)

        return a0+a1+a2+a3+a4+a5

    def _vcirc_serial(self, R, toll=1e-4):
        """Calculate the Vcirc in R using a serial code
        :param R: Cylindrical radius [kpc]
        :param toll: tollerance for quad integration
        :return:
        """
        self.set_toll(toll)

        return np.array(vcirc_einasto(R, self.d0, self.rs, self.n, self.e,self.toll))

    def _vcirc_parallel(self, R, toll=1e-4, nproc=1):
        """Calculate the Vcirc in R using a parallelized code
        :param R: Cylindrical radius [kpc]
        :param toll: tollerance for quad integration
        :param nproc: Number of processes
        :return:
        """

        self.set_toll(toll)

        pardo=ParDo(nproc=nproc)
        pardo.set_func(vcirc_alfabeta)

        htab=pardo.run_grid(R,args=(self.d0, self.rs, self.n, self.e,self.toll),_sorted='input')

        return htab



    def _dens(self, R, Z=0):

        q2 = 1 - self.e * self.e

        m = np.sqrt(R * R + Z * Z / q2)

        x = m / self.rc

        dnn=self.dn(self.n)

        A=x**(1/self.n)

        ret=self.d0*np.exp(-dnn*A)

        return ret

    def _mass(self,m):

        raise NotImplementedError()

    def __str__(self):

        s=''
        s+='Model: %s\n'%self.name
        s+='d0: %.2e Msun/kpc3 \n'%self.d0
        s+='de: %.2e Msun/kpc3 \n'%self.de
        s+='rs: %.2f\n'%self.rc
        s+='n: %.2f\n'%self.n
        s+='e: %.3f \n'%self.e
        s+='mcut: %.3f \n'%self.mcut

        return s

class valy_halo(halo):

    def __init__(self,rb,d0=None,mass=None,e=0,mcut=100):
        """
        dens=d0*exp(-m^2/rb^2)
        where d0=mass/((2 pi)^(3/2) r_b^3)
        one between d0 and mass must be set, d0 has the priority
        :param rb: Scale radius
        :param d0: central density in Msun/kpc^3
        :param mass: total mass in Msun
        :param e: eccentricity (e=0, spherical)
        :param mcut:  elliptical radius where dens(m>mcut)=0
        """

        cost_mass=((2*np.pi)**1.5)*(rb*rb*rb)*(np.sqrt(1-e*e))

        if (d0 is None) and (mass is None):
            raise ValueError('d0 or mass must be set')
        elif mass is None:
            mass=d0*cost_mass
        else:
            d0=mass/cost_mass

        super(valy_halo,self).__init__(d0=d0,rc=rb,e=e,mcut=mcut)
        self.name='Valy halo'
        self.mass=mass
        self.rb=rb

    def _potential_serial(self, R, Z, grid=False, toll=1e-4, mcut=None):
        """Calculate the potential in R and Z using a serial code

        :param R: Cylindrical radius [kpc]
        :param Z: Cylindrical height [kpc]
        :param grid:  if True calculate the potential in a 2D grid in R and Z
        :param toll: tollerance for quad integration
        :param mcut: elliptical radius where dens(m>mcut)=0
        :return:
        """


        self.set_toll(toll)

        return potential_valy(R, Z, d0=self.d0, rb=self.rc, e=self.e, mcut=mcut, toll=self.toll, grid=grid)


    def _potential_parallel(self, R, Z, grid=False, toll=1e-4, mcut=None, nproc=2):
        """Calculate the potential in R and Z using a parallelized code.

        :param R: Cylindrical radius [kpc]
        :param Z: Cylindrical height [kpc]
        :param grid:  if True calculate the potential in a 2D grid in R and Z
        :param toll: tollerance for quad integration
        :param mcut: elliptical radius where dens(m>mcut)=0
        :return:
        """

        self.set_toll(toll)


        pardo=ParDo(nproc=nproc)
        pardo.set_func(potential_valy())

        if len(R)!=len(Z) or grid==True:

            htab=pardo.run_grid(R,args=(Z,self.d0, self.rc,self.e, mcut,self.toll,grid),_sorted='sort')

        else:

            htab = pardo.run(R,Z, args=(self.d0, self.rc, self.e, mcut, self.toll, grid),_sorted='input')


        return htab

    def _vcirc_serial(self, R, toll=1e-4):
        """Calculate the Vcirc in R using a serial code
        :param R: Cylindrical radius [kpc]
        :param toll: tollerance for quad integration
        :return:
        """
        self.set_toll(toll)

        return np.array(vcirc_valy(R, self.d0, self.rc, self.e, toll=self.toll))


    def _vcirc_parallel(self, R, toll=1e-4, nproc=1):
        """Calculate the Vcirc in R using a parallelized code
        :param R: Cylindrical radius [kpc]
        :param toll: tollerance for quad integration
        :param nproc: Number of processes
        :return:
        """

        self.set_toll(toll)

        pardo=ParDo(nproc=nproc)
        pardo.set_func(vcirc_valy)

        htab=pardo.run_grid(R,args=(self.d0, self.rc, self.e, self.toll),_sorted='input')

        return htab

    def _dens(self, R, Z=0):

        q2 = 1 - self.e * self.e

        m = np.sqrt(R * R + Z * Z / q2)

        x = m / self.rc



        return self.d0*np.exp(-0.5*x*x)

    def _mass(self,m):

        raise NotImplementedError()

    def __str__(self):

        s=''
        s+='Model: %s\n'%self.name
        s+='Mass: %.2e Msun \n'%self.mass
        s+='d0: %.2e Msun/kpc3 \n'%self.d0
        s+='rb: %.2f kpc\n'%self.rc
        s+='e: %.3f \n'%self.e
        s+='mcut: %.3f \n'%self.mcut

        return s

class exponential_halo(halo):

    def __init__(self,rb,d0=None,mass=None,e=0,mcut=100):
        """
        dens=d0*exp(-m/rb)
        where d0=mass/(8*PI*rb^3)
        one between d0 and mass must be set, d0 has the priority
        :param rb: Scale radius
        :param d0: central density in Msun/kpc^3
        :param mass: total mass in Msun
        :param e: eccentricity (e=0, spherical)
        :param mcut:  elliptical radius where dens(m>mcut)=0
        """

        cost_mass=8*np.pi*rb*rb*rb*(np.sqrt(1-e*e))

        if (d0 is None) and (mass is None):
            raise ValueError('d0 or mass must be set')
        elif mass is None:
            mass=d0*cost_mass
        else:
            d0=mass/cost_mass

        super(exponential_halo,self).__init__(d0=d0,rc=rb,e=e,mcut=mcut)
        self.name='Exponential halo'
        self.mass=mass
        self.rb=rb

    def _potential_serial(self, R, Z, grid=False, toll=1e-4, mcut=None):
        """Calculate the potential in R and Z using a serial code

        :param R: Cylindrical radius [kpc]
        :param Z: Cylindrical height [kpc]
        :param grid:  if True calculate the potential in a 2D grid in R and Z
        :param toll: tollerance for quad integration
        :param mcut: elliptical radius where dens(m>mcut)=0
        :return:
        """


        self.set_toll(toll)

        return potential_exponential(R, Z, d0=self.d0, rb=self.rc, e=self.e, mcut=mcut, toll=self.toll, grid=grid)


    def _potential_parallel(self, R, Z, grid=False, toll=1e-4, mcut=None, nproc=2):
        """Calculate the potential in R and Z using a parallelized code.

        :param R: Cylindrical radius [kpc]
        :param Z: Cylindrical height [kpc]
        :param grid:  if True calculate the potential in a 2D grid in R and Z
        :param toll: tollerance for quad integration
        :param mcut: elliptical radius where dens(m>mcut)=0
        :return:
        """

        self.set_toll(toll)


        pardo=ParDo(nproc=nproc)
        pardo.set_func(potential_exponential())

        if len(R)!=len(Z) or grid==True:

            htab=pardo.run_grid(R,args=(Z,self.d0, self.rc,self.e, mcut,self.toll,grid),_sorted='sort')

        else:

            htab = pardo.run(R,Z, args=(self.d0, self.rc, self.e, mcut, self.toll, grid),_sorted='input')


        return htab

    def _vcirc_serial(self, R, toll=1e-4):
        """Calculate the Vcirc in R using a serial code
        :param R: Cylindrical radius [kpc]
        :param toll: tollerance for quad integration
        :return:
        """
        self.set_toll(toll)

        return np.array(vcirc_exponential(R, self.d0, self.rc, self.e, toll=self.toll))


    def _vcirc_parallel(self, R, toll=1e-4, nproc=1):
        """Calculate the Vcirc in R using a parallelized code
        :param R: Cylindrical radius [kpc]
        :param toll: tollerance for quad integration
        :param nproc: Number of processes
        :return:
        """

        self.set_toll(toll)

        pardo=ParDo(nproc=nproc)
        pardo.set_func(vcirc_exponential)

        htab=pardo.run_grid(R,args=(self.d0, self.rc, self.e, self.toll),_sorted='input')

        return htab

    def _dens(self, R, Z=0):

        q2 = 1 - self.e * self.e

        m = np.sqrt(R * R + Z * Z / q2)

        x = m / self.rc



        return self.d0*np.exp(-x)

    def _mass(self,m):

        raise NotImplementedError()

    def __str__(self):

        s=''
        s+='Model: %s\n'%self.name
        s+='Mass: %.2e Msun \n'%self.mass
        s+='d0: %.2e Msun/kpc3 \n'%self.d0
        s+='rb: %.2f kpc\n'%self.rb
        s+='e: %.3f \n'%self.e
        s+='mcut: %.3f \n'%self.mcut

        return s
'''