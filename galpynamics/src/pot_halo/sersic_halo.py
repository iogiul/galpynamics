from __future__ import division, print_function
from .pot_c_ext.sersic_halo import potential_sersic
import multiprocessing as mp
from ..pardo.Pardo import ParDo
from ..utility import cartesian
import numpy as np
import sys
from .pot_halo import halo
from scipy.special import gamma,gammainc
import warnings



class sersic_halo(halo):

    def __init__(self,d0,Re,n,e=0,mcut=100):
        """
        Model of halo based on the work of  Terzic&Graham, 2005, https://ui.adsabs.harvard.edu/abs/2005MNRAS.362..197T/abstract (spherical model)
        This is an analytic approximation of a 3D density profile for which the projected profile follows a Sersic profile (
        assuming spherical simmetry
        :param d0: Central density [Msun/kpc^3]
        :param Re: effective half-light radius [kpc]
        :param n: Sersic exponent
        :param e: halo ellipticity
        :param mcut:  ellipsoidal coordinate at which we assume the density goes to 0
        """

        if (n<0): raise ValueError("Sersic halo: n must be larger than 0")

        if (e!=0): warnings.warn("You are setting a non-spherical Sersic halo. The Sersic halo is based on the model by Terzic&Graham, 2005 that is made considering a deprojection "
                                 "of the Sersic profile assuming a spherical simmetry. It is possible to define a non spherical halo, but it is not assured that its projection follows a Sersic profile.")

        super(sersic_halo,self).__init__(d0=d0,rc=Re,e=e,mcut=mcut)

        self.Re = Re
        self.n = n
        self.name = "Sersic halo"
        self.b = sersic_halo.bn(self.n)
        self.p = sersic_halo.pn(self.n)
        self.Mtot = self.mmass(self.mcut)

    @classmethod
    def mtot(cls, Mtot,Re,n,e=0,mcut=100,mass_at_infinity=False):
        """
        Class method to inizialise the class starting from the total mass instead of
        central density
        :param Mtot: Total mass  [Msun]
        :param Re: effective half-light radius [kpc]
        :param n: Sersic exponent
        :param e: halo ellipticity
        :param mcut:  ellipsoidal coordinate at which we assume the density goes to 0
        :param mass_at_infinity: If False,  Mtot is the mass of the halo at mcut,
                                 It True, Mtot is considered to be the mass at infinity  (in this case
                                 the density does not go to 0 at mcut).
                                 WARNING: If True, the final profile is not self consistent
                                 since the potential and the Vcirc is obtained assuming that
                                 that d=0 for m>mcut
        :return:  A Serisc halo class
        """
        #Tempo class assuming d0=1 Msun/kpc^3
        tmpc=cls(d0=1,Re=Re,n=n,e=e,mcut=mcut)
        #Estimate total mass (assuminng d0=1, i.e. normalised over d0)
        if mass_at_infinity: Mtot_estimated = tmpc.mmass(np.inf) #Total mass at infinity
        else: Mtot_estimated = tmpc.Mtot #Total mass at mcut

        #Now estimate d0 to get the total mass in input
        d0 = Mtot/Mtot_estimated


        return cls(d0=d0, Re=Re, n=n, e=e, mcut=mcut)


    def _mass(self,m):
        """
        Mass of the halo combining Eq. A2 in Terzic&Graham, 2005, https://ui.adsabs.harvard.edu/abs/2005MNRAS.362..197T/abstract (spherical model)
        and Eq. 2.115 in Binney&Tremain (2nd edition) to take into account the halo ellipticity
        :param m: halo spheroidal coordinate
        :return: total mass within the spheroid defined by m
        """
        m=np.atleast_1d(m)

        Re3   =  self.Re*self.Re*self.Re
        exfp  =  self.n*(3-self.p)
        q = np.sqrt(1-self.e*self.e) #Correction from Eq. 2.115 in Binney&Tremain (2nd edition) to take into account the halo ellipticity
        norm  =  4*np.pi*q*self.d0*Re3*self.n*self.b**(-exfp)
        x     =  self.b*(m/self.Re)**(1/self.n)

        Masstoinf = gamma(exfp) #Integrate up to infinity

        #Notice that in scipy the lower incomplete gamma function is normalised over the gamma function
        retm = np.where(np.isposinf(m),Masstoinf,gammainc(exfp,x)*Masstoinf)

        if (len(retm)==1): return norm*retm[0]
        else: return norm*retm


    def _mdens(self,m):
        """
        Equation 4 in Terzic&Graham, 2005, https://ui.adsabs.harvard.edu/abs/2005MNRAS.362..197T/abstract
        :param R: R, cylindrical radius  [kpc] (could be int, float, array)
        :param Z: z, cylindrical height  [kpc]  (could be int, float, array)
        :return: density [Msun/kpc^3] at R,Z
        """

        x= m/self.Re

        plaw = x**(-self.p)
        Eexp = self.b*x**(1./self.n)

        return self.d0*plaw*np.exp(-Eexp)



    @staticmethod
    def bn(n):
        """
        Sersic b(n) function approximation following https://en.wikipedia.org/wiki/Sérsic_profile
        :param n: Sersic exponent n
        :return: b(n)
        """

        B = 2
        C = -1. / 3.
        A1 = 4. / 405.
        A2 = 46. / 25515.
        A3 = 131. / 1148175.
        A4 = -2194697. / 30690717750.
        n2 = n * n
        n3 = n2 * n
        n4 = n3 * n

        return B * n + C + (A1 / n) + (A2 / n2) + (A3 / n3) + (A4 / n4)

    @staticmethod
    def pn(n):
        """
        p(n) function representing the power law exponent of the 3D Sersic profile.
        This functional form is presented in Terzic&Graham, 2005, https://ui.adsabs.harvard.edu/abs/2005MNRAS.362..197T/abstract
        The authors says that this is valid to reproduce the Sersic profile in the range 0.01<R/Re<1000 and
        0.6 < n < 10.
        :param n: Sersic exponent n
        :return: p(n)
        """
        invn = 1. / n

        return 1.0 - 0.6097 * invn + 0.05563 * invn * invn

    def _potential_serial(self,R,Z,grid=False,toll=1e-4,mcut=None):
        #TODO mcut cannot be None, it is dangerous, it is true that it is handled
        #from the parent class halo, and it cannot be really mcut=None
        #but still we should remove the None
        """
        Calculate the potential in R and Z using a serial code
        :param R: Cylindrical radius [kpc] (could be int, float, 1D array)
        :param Z: Cylindrical height [kpc] (could be int, float, 1D array)
        :param grid: if True calculate the potential in a 2D grid in R and Z (all R and Z combinations)
        :param toll: tollerance for quad integration
        :param mcut: elliptical radius where dens(m>mcut)=0
        :return: Potential at R-Z
        """

        self.set_toll(toll)

        return potential_sersic(R, Z, d0=self.d0, Re=self.Re, n=self.n, e=self.e, mcut=mcut, toll=self.toll, grid=grid)

    def _potential_parallel(self, R, Z, grid=False, toll=1e-4, mcut=None,nproc=2):
        """
        Calculate the potential in R and Z using a parallelized code.
        :param R: Cylindrical radius [kpc]
        :param Z: Cylindrical height [kpc]
        :param grid:  if True calculate the potential in a 2D grid in R and Z
        :param toll: tollerance for quad integration
        :param mcut: elliptical radius where dens(m>mcut)=0
        :param nproc: number of parallel process to use
        :return:
        """
        self.set_toll(toll)

        pardo=ParDo(nproc=nproc)
        pardo.set_func(potential_sersic)

        if len(R) != len(Z) or grid == True:
            htab=pardo.run_grid(R,args=(Z,self.d0,self.Re,self.n,self.e, mcut,self.toll,grid),_sorted='sort')
        else:
            htab = pardo.run(R,Z, args=(Z,self.d0,self.Re,self.n,self.e, mcut,self.toll,grid),_sorted='input')

        return htab


    #TODO Still to be implemented
    def potential_spherical(self,R,Z,mcut=None):
        """
        For a spherical distribution, there is a seminalaytic equation for the potential
        i.e., Eq. B1 but assuming that rb=0 in  Terzic&Graham, 2005, https://ui.adsabs.harvard.edu/abs/2005MNRAS.362..197T/abstract
        Phi(r) = -(4 pi G)/r  int^r_0 dens(s) s^2 ds -(4 pi G) int^mcut_r dens(s) s ds)
        The first term is just the mass up to r multplied by G and divided by r
        the solution of the second term is
            int^mcut_r dens(s) s df =  n*Re*Re * b^(-exfac) * (Gamma_inc_upper(exfac,b*(r/Re)^1/n)-Gamma_inc_upper(exfac,b*(mcut/Re)^1/n)),
            where exfac=n*(2-p)
            so
        Phi(r) = -G*M(r)/r - 4*pi*G*n*d0*Re*Re*(Gamma_inc_upper(exfac,b*(r/Re)^1/n)-Gamma_inc_upper(exfac,b*(mcut/Re)^1/n))
        :param R: Cylindrical radius R [kpc]
        :param Z: Cylindrical height Z [kpc]
        :return: density at R, Z for a spherical distribution
        """
        if self.e>0: raise ValueError("%s: potential_spherical not allowed for non spherical halos "%self.name)

        r=np.sqrt(R*R+Z*Z)

        partA = self.mmass(r)/r

        exfac=self.n*(2-self.p)
        partBnorm=4*np.pi*self.n*self.Re*self.Re*self.d0*self.b**(-exfac)
        partB = partBnorm*0

        return self.G*(-partA-partB)


    def _vcirc_serial(self, R, toll=1e-4,mcut=None):
        """Calculate the Vcirc in R using a serial code
        :param R: Cylindrical radius [kpc]
        :param toll: tollerance for quad integration
        :param mcut: elliptical radius where dens(m>mcut)=0
        :return:
        """

        if self.e!=0: raise NotImplementedError("vcirc for non spherical halo not yet implemented for the Sersic halo mode. "
                                                "You can use the general purpose vcircR function")

        self.set_toll(toll)
        if mcut is None: mcut=self.mcut

        if isinstance(R, float) or isinstance(R, int):
            return self.vcirc_spherical(R,mcut=mcut)
        else:
            ret=np.zeros(shape=(len(R),2))
            ret[:,0]=R
            ret[:,1]=self.vcirc_spherical(R,mcut=mcut)

            return ret


    def _vcirc_parallel(self, R, toll=1e-4, nproc=1,mcut=None):
        """Calculate the Vcirc in R using a parallelized code
        :param R: Cylindrical radius [kpc]
        :param toll: tollerance for quad integration
        :param nproc: Number of processes
        :param mcut: elliptical radius where dens(m>mcut)=0
        :return:
        """

        if self.e!=0: raise NotImplementedError("Parallel vcirc not yet implemented for the Sersic halo model. "
                                                "You can use the general purpose vcircR function")



    def __str__(self):

        s=''
        s+='Model: Sersic (Terzic&Graham2015) %s\n'%self.name
        s+='d0: %.2e Msun/kpc3 \n'%self.d0
        s+='Re: %.2f kpc \n'%self.Re
        s+='n: %.1f\n'%self.n
        s+='e: %.3f \n'%self.e
        s+='mcut: %.3f kpc \n'%self.mcut
        s+='mtot at mcut: %.3e Msun \n' % self.Mtot

        return s