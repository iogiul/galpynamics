# galpynamics
Dynamics of spheroidal and disc galactic components 



**WARNING:** galpynamics and its documentation are still under development.  Some examples can be found in the folder 'Tutorials'.
Comments and suggestions are welcome!
If you are interested in using it and/or in introducing new functionalities you can send me an email (giuliano.iorio.astro@gmail.com).



## Hints 

### Parallel execution

If you are using a Python version >3.6  and you wants to use more than one process to estiamte the potential, the vcirc 
or the scale height, you have to put your script inside the if __name__="__main__" condition. 
E.g. 
```python 
import numpy as np
import galpynamics
import galpynamics.dynamic_component as dc
from galpynamics.dynamics import galpotential, discHeight
import matplotlib.pyplot as pl
import matplotlib as mpl

if __name__=="__main__":
    # CASE 2
    M200=10**10.2
    c=0.3
    rc=5.1
    r200=(3*M200/(4*np.pi*200*136.05) )**(1./3.)
    gc=1/(np.log(1+c)-c/(1+c))
    d0=M200*gc/(4*np.pi*rs**3)
    mcut=1000.
    e=0.
    dm_halo=dc.NFW_halo(d0=d0, rs=rs, mcut=mcut, e=e)


    h_test=discHeight(dynamic_components=dm_halo,disc_component=dc.Exponential_disc.thin(sigma0=1e8, Rd=3))
    h_stuff=h_test.height(vdisp=10.,Rrange=(0.001,10),Zrange=(0.001,5),Rcut=None,zcut=None,mcut=None,Rlimit='max',inttoll=1e-4,nproc=4)      

```


### Properly setting the Z-grid for the estimate of the disc thickness 

In order to estimate the vertical profile of a gaseous disc, the user need to specify a grid in R and Z. 
In particular, the grid in Z is fundamental to have a correct estimate of the gas density. If the grid spacing is too large with respect to the expected scale height only few points will be significantly larger than 0, therefore the fit of the vertical profile will be seriuosly biased and the final results could be totally wrong. 
To avoid such problem it is conveniente to> 

- Use a log scale spacing along the Z axis (Zinterval='log' in the function height, notice that log is the default value). This will help to concentrate the points where most of the density is larger than 0 making the fit of the vertial profile more robust. 

- Do not use a a too large Zmax. Again a too large Zmax means that most of the points will be very small. 

In general, always give a look to the diagnostic plot, if the vertical profile is not well sampled in the grid change the Z-grid parameters