import galpynamics
from galpynamics.dynamic_component import sersic_halo
import matplotlib.pyplot as plt
import numpy as np
from matplotlib.colors import LogNorm
from galpynamics.utility import kpcMyr_to_kms

print(galpynamics.__version__)

d0=1e9
Re=5
n=2.8

sh=sersic_halo(d0=d0,Re=Re,n=n,e=0.,mcut=100)

sh2=sersic_halo.mtot(Mtot=sh.Mtot,Re=Re,n=n,e=0,mcut=100)
sh3=sersic_halo.mtot(Mtot=sh.Mtot,Re=Re,n=n,e=0,mcut=100,mass_at_infinity=True)

print(sh)
print(sh2)
print(sh3)

mm=np.logspace(-2,2)
plt.plot(mm,sh.mmass(mm))
plt.xscale("log")
plt.yscale("log")
plt.show()

RR=np.logspace(-2,1,100)
zz=np.logspace(-2,1,50)

pot=sh.potential(RR,zz,grid=True,output="2D")


print(pot)
print(pot.shape)
plt.imshow(np.abs(pot[2].T),origin="lower",extent=[0.01,100,0.01,100],norm=LogNorm(),cmap="plasma")
plt.colorbar()
plt.show()
#plt.pcolormesh(pot[:,0],pot[:,1],pot[:,2])
#plt.show()


def FR(fmod,R,Z,grid=False,hrel=1e-5):

    hrelR=hrel*R

    Rup  = R+hrelR
    Rlow = R-hrelR

    FRup  = fmod.potential(Rup,Z=Z,grid=grid)
    FRlow = fmod.potential(Rlow,Z=Z,grid=grid)

    res = np.zeros_like(FRup)
    res[:,0] = FRup[:,0] - hrelR
    res[:,1] = FRup[:,1]
    res[:,2] = np.where(R==0,0,(FRup[:,2] - FRlow[:,2])/(FRup[:,0]-FRlow[:,0]))


    return res

s113=sersic_halo.mtot(Mtot=6e10, Re=1.4,n=6,e=0,mcut=100)
print(s113)
rr=np.linspace(0.001,4,1000)
pot=s113.potential(rr,Z=0,grid=True)

vvc=s113.vcircR(R=rr,Z=0,grid=True)[:,2]
#vvc=np.sqrt(rr*FR(s113,rr,Z=0,grid=True)[:,2])*kpcMyr_to_kms
Vcirc=s113.vcirc_spherical(rr)
plt.plot(rr,Vcirc,lw=4,label="Semi-analytic circular")
plt.plot(rr[:],vvc[:],ls="dashed",lw=4,label="Integrated circular")
plt.ylim(0,450)

s113e=sersic_halo.mtot(Mtot=6e10, Re=1.4,n=6,e=0.9,mcut=100)
vvc=s113e.vcircR(R=rr,Z=0,grid=True)[:,2]


#vvc=np.sqrt(rr*FR(s113e,rr,Z=0,grid=True)[:,2])*kpcMyr_to_kms
plt.plot(rr[:],vvc[:],ls="dashed",lw=4,label="Integrated flattened (e=0.9)")


vvcZ=s113e.vcircR(R=rr,Z=0.01,grid=True)[:,2]
plt.plot(rr[:],vvcZ[:],ls="dotted",lw=4,label="Integrated flattened (e=0.9) at Z=0.01 kpc")
vvcZ=s113e.vcircR(R=rr,Z=0.5,grid=True)[:,2]
plt.plot(rr[:],vvcZ[:],ls="dotted",lw=4,label="Integrated flattened (e=0.9) at Z=0.5 kpc")
vvcZ=s113e.vcircR(R=rr,Z=2,grid=True)[:,2]
plt.plot(rr[:],vvcZ[:],ls="dotted",lw=4,label="Integrated flattened (e=0.9) at Z=2 kpc")
plt.legend()
plt.show()


"""
s0345=sersic_halo.mtot(Mtot=2.3e10, Re=0.3,n=1.5,e=0,mcut=100)
print(s0345)
rr=np.linspace(0.001,4,1000)
Vcirc=s0345.vcirc_spherical(rr)
plt.plot(rr,Vcirc)
plt.show()

s0441=sersic_halo.mtot(Mtot=1.8e10, Re=0.10,n=2,e=0,mcut=100)
print(s0441)
rr=np.linspace(0.001,4,1000)
Vcirc=s0441.vcirc_spherical(rr)
plt.plot(rr,Vcirc)
plt.show()

s2132=sersic_halo.mtot(Mtot=1.9e10, Re=1.4,n=0.94,e=0,mcut=100)
print(s2132)
rr=np.linspace(0.001,4,1000)
Vcirc=s2132.vcirc_spherical(rr)
plt.plot(rr,Vcirc)
plt.show()
"""

"""
plt.imshow(pot.T,origin="lower",extent=[0.01,100,0.01,100])
plt.colorbar()
#plt.xscale("log")
#plt.yscale("log")
plt.show()

print(sh.potential(2,0))
print(sh.potential(0,2))
"""

