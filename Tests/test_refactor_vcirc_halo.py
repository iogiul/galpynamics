import galpynamics
from galpynamics.dynamic_component import sersic_halo, alfabeta_halo, NFW_halo
import numpy as np
import matplotlib.pyplot as plt

RR=np.linspace(0.01,20,1000)


d0 = 1.32e7
rs = 16.47
mcut = 100
q = 1.0
e = np.sqrt(1 - q * q)

halo = NFW_halo(d0=d0, rs=rs, mcut=mcut, e=e)

vc=halo.vcirc(R=RR)
plt.plot(vc[:,0],vc[:,1])

vc=halo.vcirc(R=RR,mcut=10)
plt.plot(vc[:,0],vc[:,1])

vc=halo.vcircR(R=RR,Z=0,mcut=mcut,hrel=5e-2)
plt.plot(vc[:,0],vc[:,-1],ls="dashed")

vc=halo.vcircR(R=RR,Z=0,mcut=10,hrel=5e-2)
plt.plot(vc[:,0],vc[:,-1],ls="dashed")

plt.show()

d0=1e9
Re=5
n=2.8

sh=sersic_halo(d0=d0,Re=Re,n=n,e=0.,mcut=100)
RR=np.linspace(0.01,20,1000)
vc=sh.vcirc(R=RR)
plt.plot(vc[:,0],vc[:,1])

vc=sh.vcirc(R=RR,mcut=10)
plt.plot(vc[:,0],vc[:,1])

vc=sh.vcircR(R=RR,Z=0,mcut=10,hrel=5e-2)
plt.plot(vc[:,0],vc[:,-1],ls="dashed")

plt.show()

d0 = 9.8351e+10
rs = 0.075
alfa = 0
beta = 1.8
mcut = 100
q = 0.5
e = np.sqrt(1 - q * q)
bulge = alfabeta_halo(d0=d0, alfa=alfa, beta=beta, rs=rs, mcut=mcut, e=e)

vc=bulge.vcirc(R=RR)
plt.plot(vc[:,0],vc[:,1])

vc=bulge.vcirc(R=RR,mcut=2.1)
plt.plot(vc[:,0],vc[:,1])

vc=bulge.vcircR(R=RR,Z=0,mcut=2.1,hrel=5e-2)
plt.plot(vc[:,0],vc[:,-1],ls="dashed")

plt.show()
