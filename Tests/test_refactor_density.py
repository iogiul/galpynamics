import galpynamics
from galpynamics.dynamic_component import sersic_halo, truncated_alfabeta_halo
import matplotlib.pyplot as plt
import numpy as np
from matplotlib.colors import LogNorm
from galpynamics.utility import kpcMyr_to_kms


print("Version",galpynamics.__version__)

# truncated alfabeta halo
d0 = 3
rs = 5
alfa = 1.5
beta = 3.2
rcut = 10
mcut = 100
e = 0.5
mod=galpynamics.truncated_alfabeta_halo(d0=d0, rs=rs, alfa=alfa, beta=beta, rcut=rcut, mcut=mcut, e=e)
RR=np.linspace(0.001,1.5*mcut)
plt.plot(RR,mod.dens(RR)[:,-1],label=" No Extend over")
plt.plot(RR,mod.dens(RR,extend_over_mcut=True)[:,-1],label="Extend over",ls="dashed")
plt.legend()
plt.xscale("log")
plt.yscale("log")
plt.show()