from __future__ import print_function
import galpynamics
from timeit import default_timer as timer
import numpy as np
import sys
from termcolor import colored
import matplotlib.pyplot as plt

neval = 100
R = np.linspace(0.05, 30, neval)
Z = np.linspace(0.05, 5, neval)



#MWMcMillan17
gmod=galpynamics.MWMcMillan17()
print(gmod)
input()

Pot_serial=gmod.potential(R,Z,grid=False, toll=1e-4,nproc=1)
print(Pot_serial)



fR=gmod.forceR(R,Z,grid=False, toll=1e-4,nproc=1)
print(fR)



fZ=gmod.forceZ(R,Z,grid=False, toll=1e-4,nproc=1)


vc=gmod.vcircR(R,Z=0,grid=True, hrel=1e-2, toll=1e-4,nproc=1)
RR=vc[:,0]
print("L",vc.shape)
plt.plot(RR,vc[:,-1])

vcl=np.zeros(shape=(len(vc),vc.shape[1]-3))
vcl[:,:-1]=vc[:,2:-2]
vcl[:,-1] = vc[:,-1]
for i in range(vcl.shape[1]):
    plt.plot(vc[:,0],vcl[:,i],c=f"C0{i}",label=f"{i}")
plt.legend()

vc=gmod.vcirc(R, toll=1e-4,nproc=1)
print("F",vc.shape)
RR=vc[:,0]
plt.plot(RR,vc[:,-1],ls="dashed")
for i in range(vc.shape[1]-1):
    plt.plot(vc[:,0],vc[:,1+i],ls="dashed",c=f"C0{i}",lw=4,zorder=100)
plt.show()

#test_halo_component(_to_test, 100)