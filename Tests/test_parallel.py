from galpynamics.dynamic_component import  Exponential_disc
import numpy as np
import time

if __name__=="__main__":
    sigma0 = 1.83444e+08
    Rd = 3.02134
    zd = 0.9
    Rcut = 50
    zcut = 50
    tkd = Exponential_disc.thick(sigma0=sigma0, Rd=Rd, zd=zd, Rcut=Rcut, zcut=zcut, zlaw='exp')

    RR=np.linspace(0.01,20,100)
    ZZ=np.linspace(0.01,5,100)

    ts= time.perf_counter()
    l=tkd.potential(RR,ZZ,grid=True,nproc=6)
    print("6",time.perf_counter()-ts)

    ts= time.perf_counter()
    l=tkd.potential(RR,ZZ,grid=True,nproc=2)
    print("2",time.perf_counter()-ts)

    ts= time.perf_counter()
    l=tkd.potential(RR,ZZ,grid=True,nproc=1)
    print("1",time.perf_counter()-ts)
